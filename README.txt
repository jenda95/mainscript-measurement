***** README *****

Zde najdeš kompletní popis celého skriptu a jeho užití.

1) Struktura celého řešení:
K provedení měření je zapotřebí 3 VM:
	1. VM central ze které se to vše řídí - na které je spouštěn main.sh skript
	2. VM2 generator (sender) na které je v rootu mít zapotřebí složku: ~/automatedPerlGen/, do které se cpe run1.pl
	3. VM3 picker (reciever) na který se zprávy po vygenerování rsyslog -> rsyslog odesílají

	Pozn: Je doporučeno mít VM generátor výkonnější něž picker, aby bylo vygenerováno dostatečné množství zpráv (moje HW konf. Core i5 4CPU, 4GB RAM)

2) Zasíťování:
	
	Jsou zde zřízeny dvě sítě, aby nedocházelo k ovlivnění měření, jedna je konfigurovaná dle uživatele a její parametry jsou zadávány při spouštění skriptu, druhá je nastavena staticky a má následující hodnoty

	picker: 172.16.220.3
	generator: 172.16.220.5

3)Spuštění:
Skript se spouští následujícím příkazem
./main.sh -p udp -c 10 -s 172.16.210.7 -r 172.16.210.5 -t 1,2,3,4,5,6 -m 100,500,1000,2000,3000,4000,5000,10000

	-p udává protokol, pro oba protokoly parametr udptcp
	-c udává počet opakování jednotlivých měření
	-s udává ip adresu stroje, ze kterého se zprávy odesílají tedy VM2 generator (sender), je třeba jeho adresu vhodně nakonfigurovat
	-r udává ip adresu pickeru
	Pozn: uživatelem konfigurované adresy slouží pro komunikaci skriptu mezi central VM a VM1 a VM2, nesmí být použita stejná síť pro měření a pro komunikační síť
	-t seznam threadů pro které bude měření provedeno
	-m seznam počtu zpráv pro které bude měření provedeno


