#!/usr/bin/perl

use strict;
use warnings;
use Time::HiRes qw(gettimeofday tv_interval usleep);
use Log::Log4perl qw(:easy);

use threads;

use 5.20.0;

my $PER_SEC = 1000;

Log::Log4perl->easy_init( $ERROR );

Log::Log4perl->easy_init(
        {
        level => $ERROR,
        file  => ">> error_log",
        }
        );

my $start = [gettimeofday];
my $startMilis = gettimeofday;
my $endMilis = gettimeofday + 60;

my $counter = 0;

my $actualTime = gettimeofday;
say (gettimeofday);



while ($actualTime <= $endMilis)
{
        $counter++;
#       say("homogenni_log $counter");
        say("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nulla pulvinar eleifend sem. In rutrum. Maecenas aliquet accumsan leo. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Aliquam ante. Etiam dictum tincidunt diam. Aliquam id dolor. Nullam sit amet magna in magna gravida vehicula. Nulla est. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. $counter");
                my $elapsed = tv_interval($start);
                my $speed = $counter / $elapsed;

                if ($speed > $PER_SEC) {
                        usleep(10_000);
                }

                if ($counter % 1000 == 0) {
                        # print STDERR ("Elapsed: $elapsed (speed: $speed / sec)\n");
                        ERROR( "Elapsed: $elapsed (speed: $speed / sec)\n" );

                }

                $actualTime = gettimeofday;
}
say (gettimeofday);
exit;