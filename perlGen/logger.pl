use Log::Any qw($log);
use Log::Any::Adapter;
use Log::Any::Adapter::Syslog;
my $loggerOptions;

if (POSIX::isatty(*STDOUT))  # Run from console -> syslog + stderr
{
 $runFromConsole = 1;
 $loggerOptions = Log::Any::Adapter::Syslog::LOG_PID | Log::Any::Adapter::Syslog::LOG_PERROR;
}
else  # Run from non-console -> syslog
{
 $loggerOptions = Log::Any::Adapter::Syslog::LOG_PID;
}Log::Any::Adapter->set(
 'Syslog',
 name => 'autoPayment-go',
 facility => Log::Any::Adapter::Syslog::LOG_LOCAL6,
 options => $loggerOptions,
);

$log->info("START [@ARGV]");