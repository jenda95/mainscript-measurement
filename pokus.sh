#./main.sh udp 10 172.16.210.7 172.16.210.5


VERSION=1.1
SUBJECT=666
USAGE="Usage: ./main.sh -p protocol (udp, tcp, udptcp), -c message repetition (1-10), -s sender_ip (172.16.210.7), -r reciever_ip (172.16.210.5), -t thread_list (1-6), -m message_list (100-10000)"
EXAMPLE="Example: ./main.sh -p udp -c 10 -s 172.16.210.7 -r 172.16.210.5 -t 1,2,3,4,5,6 -m 100,500,1000,2000,3000,4000,5000,10000"

# --- Options processing -------------------------------------------
if [ $# == 0 ] ; then
    echo $USAGE
    exit 1;
fi

protocol_argument=""
repetition_argument=""
sender_ip_argument=""
reciever_ip_argument=""
thread_list_argument=""
message_list_argument=""


while getopts ":p:c:s:r:t:m:vh" optname
  do
    case "$optname" in
		"v")
		echo "Version $VERSION"
		exit 0;
		;;
		"p")
		echo "-p argument: $OPTARG"
		protocol_argument=$OPTARG
		;;
		"c")
		echo "-c argument: $OPTARG"
		repetition_argument=$OPTARG
		;;
		"s")
		echo "-s argument: $OPTARG"
		sender_ip_argument=$OPTARG
		;;
		"r")
		echo "-r argument: $OPTARG"
		reciever_ip_argument=$OPTARG
		;;
		"t")
		echo "-t argument: $OPTARG"
		thread_list_argument=$OPTARG
		;;
		"m")
		echo "-m argument: $OPTARG"
		message_list_argument=$OPTARG
		;;
		"h")
		echo $USAGE
		echo $EXAMPLE
		exit 0;
		;;
		"?")
		echo "Unknown option $OPTARG"
		exit 0;
		;;
		":")
		echo "No argument value for option $OPTARG"
		exit 0;
		;;
		*)
		echo "Unknown error while processing options"
		exit 0;
		;;
    esac
  done

# shift $(($OPTIND - 1))

# param1=$1
# param2=$2

# --- Locks -------------------------------------------------------
LOCK_FILE=/tmp/$SUBJECT.lock
if [ -f "$LOCK_FILE" ]; then
   echo "Script is already running"
   exit
fi

trap "rm -f $LOCK_FILE" EXIT
touch $LOCK_FILE

# --- Body --------------------------------------------------------
#  SCRIPT LOGIC GOES HERE
# echo $param1
# echo $param2
echo $protocol_argument
echo $repetition_argument
# -----------------------------------------------------------------