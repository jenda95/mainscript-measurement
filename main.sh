#!/bin/bash   

#command: ./main.sh
#command ex.: ./main.sh -p udp -c 10 -s 172.16.210.7 -r 172.16.210.5 -t 1,2,3,4,5,6 -m 100,500,1000,2000,3000,4000,5000,10000

# Jan Kohout
# 13.08.2019
# soubory: tcp514.conf, udp514.conf
# CHANGELOG
# 13.08.2019 - vytvoren soubor, zakladni kostra, parametry
# 14.08.2019 - argument handling
# 15.08.2019 - restrukturalizace kodu, run1.pl

#posloupnost skriptu
#1. for vybira moznosti protokolu (tcp, udp), jsou 2
#2. for vybira moznosti threadu 1-6, 6
#3. for vybira moznosti poctu zprav (100-10000), 8
#4. for provadi 10x to same mereni pro overeni spravnosti, 10
# 2*6*8*10 moznych mereni tedy
#cele mereni / cely skript je zapotrebi spustit dvakrat, nebot je treba zmerit jeste moznosti pro 1 CPU jadro, tedy *2
#dohromady tedy 2*6*8*10*2=1920 csv souboru
#

VERSION=1.1
SUBJECT="main.sh"
USAGE="Usage: ./main.sh -p protocol (udp, tcp, udptcp), -c message repetition (1-10), -s sender_ip (172.16.210.7), -r reciever_ip (172.16.210.5), -t thread_list (1-6), -m message_list (100-10000)"
EXAMPLE="Example: ./main.sh -p udp -c 10 -s 172.16.210.7 -r 172.16.210.5 -t 1,2,3,4,5,6 -m 100,500,1000,2000,3000,4000,5000,10000"

declare -a ALLES_DSTAT_PIDS
declare -a MEAS_DSTAT_PIDS

# --- Options processing -------------------------------------------
if [ $# == 0 ] ; then
    echo $USAGE
    exit 1;
fi

protocol_argument=""
repetition_argument=""
sender_ip_argument=""
reciever_ip_argument=""
thread_list_argument=""
message_list_argument=""

logger -t $SUBJECT$VERSION "Working on arguments:"
echo "Working on arguments:"

while getopts ":p:c:s:r:t:m:vh" optname
  do
    case "$optname" in
        "v")
        echo "Version $VERSION"
        exit 0;
        ;;
        "p")
        echo "-p argument: $OPTARG"
        protocol_argument=$OPTARG
        ;;
        "c")
        echo "-c argument: $OPTARG"
        repetition_argument=$OPTARG
        ;;
        "s")
        echo "-s argument: $OPTARG"
        sender_ip_argument=$OPTARG
        ;;
        "r")
        echo "-r argument: $OPTARG"
        reciever_ip_argument=$OPTARG
        ;;
        "t")
        echo "-t argument: $OPTARG"
        thread_list_argument=$OPTARG
        ;;
        "m")
        echo "-m argument: $OPTARG"
        message_list_argument=$OPTARG
        ;;
        "h")
        echo $USAGE
        echo $EXAMPLE
        exit 0;
        ;;
        "?")
        echo "Unknown option $OPTARG"
        exit 0;
        ;;
        ":")
        echo "No argument value for option $OPTARG"
        exit 0;
        ;;
        *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done

# --- Handle missing arguments -----------------------------------
if [ "$protocol_argument" = "" ];
then
    echo "Missing -p argument!"
    exit 0;
elif [[ "$repetition_argument" = "" ]]; then
    echo "Missing -c argument!"
    exit 0;
elif [[ "$sender_ip_argument" = "" ]]; then
    echo "Missing -s argument!"
    exit 0;
elif [[ "$reciever_ip_argument" = "" ]]; then
    echo "Missing -r argument!"
    exit 0;
elif [[ "$thread_list_argument" = "" ]]; then
    echo "Missing -t argument!"
    exit 0;
elif [[ "$message_list_argument" = "" ]]; then
    echo "Missing -m argument!"
    exit 0;
fi


# --- Locks -------------------------------------------------------
LOCK_FILE=/tmp/$SUBJECT.lock
if [ -f "$LOCK_FILE" ]; then
   echo "Script is already running"
   exit
fi

trap "rm -f $LOCK_FILE" EXIT
touch $LOCK_FILE

# --- Argument translation for script flow ------------------------

logger -t $SUBJECT $VERSION "Translating arguments:"
echo ""
echo "Translating arguments:"

case $protocol_argument in
    tcp) declare -i iprotocol=1 
         declare -i iprotocolMax=2 ;;
    udp) declare -i iprotocol=2 
         declare -i iprotocolMax=3 ;;
    udptcp) declare -i iprotocol=1
            declare -i iprotocolMax=3 ;;
    tcpudp) declare -i iprotocol=1
            declare -i iprotocolMax=3 ;;
    *)
        echo "Wrong -p argument! Choices: tcp, udp, udptcp, tcpudp"
        exit 0;
        ;;
esac

repetitionRegex=$(awk '/[0-9]/' <<< $repetition_argument)
echo "$repetitionRegex"
if [[ "$repetitionRegex" == "" ]];
then
    echo "Wrong -c parameter! Example: 1 or 10"
    exit 0;
fi

senderIpregex=$(awk '/(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])/' <<< $sender_ip_argument)
echo "$senderIpregex"
if [[ "$senderIpregex" == "" ]];
then
    echo "Wrong -s parameter! Example:  192.168.1.2"
    exit 0;
fi

recieverIpregex=$(awk '/(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])/' <<< $reciever_ip_argument)
echo "$recieverIpregex"
if [[ "$recieverIpregex" == "" ]];
then
    echo "Wrong -r parameter! Example:  192.168.1.3"
    exit 0;
fi

declare -a thread_list
regexComparsion=$(awk '/[A-Za-z]/' <<< $thread_list_argument)
echo "$regexComparsion"
if [[ "$regexComparsion" = "" ]]; 
then
    if [[ "$thread_list_argument" == *","* ]];
    then
        IFS=',' read -r -a thread_list <<< "$thread_list_argument"
    else
        thread_list[0]=$thread_list_argument
    fi

    for element in "${thread_list[@]}"
    do
        echo "$element"
    done
else
    echo "Wrong -t argument! Right example: 1,2,3 or 1"
    exit 0;
fi

declare -a message_count_list
regexComparsion2=$(awk '/[A-Za-z]/' <<< $message_list_argument)
echo "$regexComparsion2"
if [[ "$regexComparsion2" = "" ]]; 
then
    if [[ "$message_list_argument" == *","* ]];
    then
        IFS=',' read -r -a message_count_list <<< "$message_list_argument"
    else
        message_count_list[0]=$message_list_argument
    fi

    for element in "${message_count_list[@]}"
    do
        echo "$element"
    done
else
    echo "Wrong -m argument! Right example: 100,2000,5000 or 100"
    exit 0;
fi

# -- translated variables --
#$repetition_argument
#$sender_ip_argument
#$reciever_ip_argument
#$thread_list
#$message_count_list

echo ""

# --- functions ---------------------------------------------------

#func: restartRsyslog
#params: [String] destination (ip address), 
restartRsyslog () {
    local restartConfirmed=false
    local -i counter=0
    while [[ $restartConfirmed == false && $counter < 10 ]]; do
        ssh root@$1 'systemctl restart rsyslog; exit;'
        
        for (( q=1; q<=10; q++ )); do
            echo "waiting for rsyslog $1 restart done ... $q s"
            sleep 1
        done
        
        local restarted=$(ssh root@$1 'ps -eo etimes,pid,cmd | grep rsyslogd')
        local restarted=$(echo $restarted | awk '{ print $1 }')
        
        if [[ $restarted < 50 ]]; 
        then
            echo $restarted
            echo "Rsyslog on $1 restarted successfuly before $restarted s!"
            restartConfirmed=true
        else
            echo "Rsyslog was not restarted, there was a unknown system problem, trying to restart it again!"
        fi
        counter="$counter"+1
    done     
}


#func: checkListeningPorts
#params: [String] destination (ip address),
checkListeningPorts () {
    echo $(ssh root@$1 'lsof -i -P -n | grep 514')
    #TODO: dodelat automatickou ochranu jestli opravdu nasloucha
}

#func: deleteOldConfigs
#params: [String] destination (ip address), [String] pathToConfigFile (directory, where it is with slash on the end), [String] nameOfConfigFile, 
deleteFiles() {
    # /etc/rsyslog.d/
    local fileCheck=$(ssh root@$1 'ls '"$2"' | grep '"$3"'; exit;')
    echo "$fileCheck"
    if [[ $fileCheck == "$3" ]];
    then
        ssh root@$1 'rm -r '"$2""$3"'; exit;'
        echo "Old files on $1 with name $2$3 were deleted!"
    else
        echo "No old files on $1 with name $2$3 were found, program continues ..."
    fi
}

#func: remoteCopyFile
#params: [String] destination (ip address), [String] pathToConfigFile (directory, where it is with slash on the end), [String] nameOfConfigFile, [String] destinationDirectory
remoteCopyFile () {
    local -a remoteConfmd5
    local thisConf=$(md5sum "$2""$3" | awk '{ print $1 }')

    while [[ "$thisConf" != "${remoteConfmd5[0]}" ]]; do
            echo "$1"
            echo "$2""$3"

            scp "$2""$3" root@$1:"$4"
            
            remoteConf=$(ssh root@$1 'md5sum '"$4""$3"'; exit;')

            # declare -a remoteConfmd5 
            IFS=' ' read -r -a remoteConfmd5 <<< "$remoteConf"

            echo "${remoteConfmd5[0]}"
            echo "$thisConf"

            if [[ $thisConf == ${remoteConfmd5[0]} ]];
            then
                echo "$4$3 copied successfuly!"
            fi
    
            cat "$2""$3"
    done
}

#func: show rsyslog directory
#params: [String] destination (ip address), [String] directory
#/etc/rsyslog.d/
showDirectory () {
    local directory=$(ssh root@$1 'ls -a '"$2"'; exit;')
    echo "$1 $2 ::"
    echo "$directory"
}

#func: copyRun - copies run1.pl into right dir
#params: [String] destination (ip address), [String] pathToRunFile (directory, where it is with slash on the end), [String] nameOfRunFile
copyRun () {
    local -a remoteConfmd5
    local thisConf=$(md5sum "$2""$3" | awk '{ print $1 }')
 
    while [[ "$thisConf" != "${remoteConfmd5[0]}" ]]; do
            echo "$1"
            echo "$2""$3"

            scp "$2""$3" root@$1:automatedPerlGen/
            
            remoteConf=$(ssh root@$1 'md5sum automatedPerlGen/'"$3"'; exit;')

            # declare -a remoteConfmd5 
            local IFS=' ' read -r -a remoteConfmd5 <<< "$remoteConf"

            echo "${remoteConfmd5[0]}"
            echo "$thisConf"

            if [[ $thisConf == ${remoteConfmd5[0]} ]];
            then
                echo "$3 copied successfuly!"
            fi
    
            cat "$2""$3"
    done
}

# #func: showRunDirectory
# #params: [String] destination (ip address)
# showRunDirectory () {
#     local directory=$(ssh root@$1 'ls -a automatedPerlGen/; exit;')
#     echo "$1 automatedPerlGen/ ::"
#     echo "$directory"
# }

#func: startRun
#params: [String] destination (ip address)
startRun () {

    ssh root@"$1" 'perl automatedPerlGen/run1.pl | logger -t run1.pl'

} 

#func: getPID
#params: [String] destination, [String] process name, 
getPID () {

    local ps=$(ssh root@$1 'ps -eo etimes,pid,cmd | grep '"$2"'')
    local pid=$(echo $restarted | awk '{ print $2 }')
    echo "pidko"
    echo "$ps"
}

# --- script flow -------------------------------------------------

#TODO: start dstat onbackground
#ssh -n -f root@172.16.210.5 "dstat --epoch --time --net -N ens19 --net-packets --mem --swap --cpu --load --io --disk-util --disk --output HOVNO1.csv > /dev/null"
#ps -ef | grep dstat | awk '{print $2}'
# echo "Would you like to continue in alles.csv file? (y/n)"
# read input

# if [[ $input == "n" ]]; 
# then
#     deleteFiles $reciever_ip_argument "automatedMeasurements/" "alles.csv"
# fi

# echo "Starting dstat --epoch --time --net -N ens19 --net-packets --mem --swap --cpu --load --io --disk-util --disk --output automatedMeasurements/alles.csv > /dev/null"
# echo "on background ..."
# ssh -n -f root@$reciever_ip_argument 'dstat --epoch --time --net -N ens19 --net-packets --mem --swap --cpu --load --io --disk-util --disk --output automatedMeasurements/alles.csv > /dev/null'
# ALLESPIDS=$(ssh root@$reciever_ip_argument 'ps -ef | grep dstat') 
# ALLESPIDS=$(echo "$ALLESPIDS" | awk '{print $2}')
# echo $ALLESPIDS
# IFS=' ' read -r -a ALLES_DSTAT_PIDS <<< "$ALLESPIDS"

# for element in "${ALLES_DSTAT_PIDS[@]}"
#     do
#         echo "$element"
#     done

# exit 0;
# TODO zaznamenat PIDKA ALLES DSTATU pro pozdejsi kill 

# getPID $sender_ip_argument "run1.pl"

# exit 0;

#prvni cyklus pro vyber protokolu
for (( i = $iprotocol; i < $iprotocolMax; i++ )); do 
  	protocol=""
    if [ "$i" == 1 ];
    then
    	protocol="tcp"
    	echo "****** CONFIGURATION: $protocol :"
    	
        #upravit omfwd konfigurak na tcp
        sed -i -e 's/protocol="udp"/protocol="tcp"/g' configs/sender/10-omfwd.conf
        sed -i -e 's/protocol=""/protocol="tcp"/g' configs/sender/10-omfwd.conf

        #TODO: SCP nakopirovat na SENDER TCP konfiguraci -> zvalidovat pomoci MD5, restartovat rsyslog
        echo ""
        echo "****** SENDER CONFIGURATION ******"

        deleteFiles $sender_ip_argument "/etc/rsyslog.d/" "10-omfwd.conf"
        remoteCopyFile $sender_ip_argument "configs/sender/" "10-omfwd.conf" "/etc/rsyslog.d/"
        showDirectory $sender_ip_argument "/etc/rsyslog.d/"
        restartRsyslog $sender_ip_argument
        

        #TODO: SCP nakopirovat na PICKER TCP konfiguraci -> zvalidovat pomoci MD5, restartovat rsyslog
        echo ""
        echo "****** RECIEVER CONFIGURATION ******"
        
        deleteFiles $reciever_ip_argument "/etc/rsyslog.d/" "5-tcp.conf"
        deleteFiles $reciever_ip_argument "/etc/rsyslog.d/" "5-udp.conf"

        remoteCopyFile $reciever_ip_argument "configs/picker/" "5-tcp.conf" "/etc/rsyslog.d/"
        showDirectory $reciever_ip_argument "/etc/rsyslog.d/"
        restartRsyslog $reciever_ip_argument

        checkListeningPorts $reciever_ip_argument
        
    # ************ UDP KONFIGURACE ***********
    elif [[ "$i" == 2 ]]; then
    	protocol="udp"
    	echo "****** CONFIGURATION: $protocol :"

        #upravit omfwd konfigurak na tcp
        sed -i -e 's/protocol="tcp"/protocol="udp"/g' configs/sender/10-omfwd.conf
        sed -i -e 's/protocol=""/protocol="udp"/g' configs/sender/10-omfwd.conf

        #TODO: SCP nakopirovat na SENDER UDP konfiguraci -> zvalidovat pomoci MD5, restartovat rsyslog
        echo ""
        echo "****** SENDER CONFIGURATION ******"

        deleteFiles $sender_ip_argument "/etc/rsyslog.d/" "10-omfwd.conf"
        remoteCopyFile $sender_ip_argument "configs/sender/" "10-omfwd.conf" "/etc/rsyslog.d/"
        showDirectory $sender_ip_argument "/etc/rsyslog.d/"
        restartRsyslog $sender_ip_argument

        #TODO: SCP nakopirovat na PICKER UDP konfiguraci -> zvalidovat pomoci MD5, restartovat rsyslog
        echo ""
        echo "****** RECIEVER CONFIGURATION ******"
        
        deleteFiles $reciever_ip_argument "/etc/rsyslog.d/" "5-tcp.conf"
        deleteFiles $reciever_ip_argument "/etc/rsyslog.d/" "5-udp.conf"

        remoteCopyFile $reciever_ip_argument "configs/picker/" "5-udp.conf" "/etc/rsyslog.d/"
        showDirectory $reciever_ip_argument "/etc/rsyslog.d/"
        restartRsyslog $reciever_ip_argument

        checkListeningPorts $reciever_ip_argument
    fi


    # ---- THREADS MEASUREMENT ----
    for element in "${thread_list[@]}"
    do
        echo "$element"
    done


    len=${#thread_list[@]}
    
    echo "$len"
    
    for (( j = 0; j < ${#thread_list[@]}; j++ )); do

        #upravit konfigurak na pocet threadu
        sed -i -e 's/queue.workerthreads="1"/queue.workerthreads="'"${thread_list[$j]}"'"/g' configs/picker/10-thread.conf
        sed -i -e 's/queue.workerthreads="2"/queue.workerthreads="'"${thread_list[$j]}"'"/g' configs/picker/10-thread.conf
        sed -i -e 's/queue.workerthreads="3"/queue.workerthreads="'"${thread_list[$j]}"'"/g' configs/picker/10-thread.conf
        sed -i -e 's/queue.workerthreads="4"/queue.workerthreads="'"${thread_list[$j]}"'"/g' configs/picker/10-thread.conf
        sed -i -e 's/queue.workerthreads="5"/queue.workerthreads="'"${thread_list[$j]}"'"/g' configs/picker/10-thread.conf
        sed -i -e 's/queue.workerthreads="6"/queue.workerthreads="'"${thread_list[$j]}"'"/g' configs/picker/10-thread.conf

        echo "Thread config edited successfully!"

        deleteFiles $reciever_ip_argument "/etc/rsyslog.d/" "10-thread.conf"
        
        remoteCopyFile $reciever_ip_argument "configs/picker/" "10-thread.conf" "/etc/rsyslog.d/"
        showDirectory $reciever_ip_argument "/etc/rsyslog.d/"
        restartRsyslog $reciever_ip_argument

        for (( k = 0; k < ${#message_count_list[@]}; k++ )); do

            sed -i -e 's/my $PER_SEC = .*;/my $PER_SEC = '"${message_count_list[$k]}"';/g' perlGen/run1.pl

            deleteFiles $sender_ip_argument "~/automatedPerlGen/" "run1.pl"

            remoteCopyFile $sender_ip_argument "perlGen/" "run1.pl" "~/automatedPerlGen/"

            showDirectory $sender_ip_argument "~/automatedPerlGen/"

          for (( r = 1; r <= $repetition_argument; r++)); do
              echo "${r} measurement repetition"
              startRun $sender_ip_argument
              sleep 5

          done
          
        done


    done
done
        	
   